NAME = philo
CC = gcc
FLAGS = -Wall -Werror -Wextra
THREADS = -pthread
HEADER = includes
SRC = sources
OBJ = objects
SOURCES =	main.c \
			philosophers.c \
			forks.c \
			tasks.c \
			threads.c \
			alive.c \
			time.c \
			parsing.c \
			writing.c \
			checks.c \
			error.c \
			utils.c \
			exit.c \
			debug.c
SRCS = $(addprefix $(SRC)/, $(SOURCES))
OBJS = $(addprefix $(OBJ)/, $(SOURCES:.c=.o))

all: $(OBJ) $(NAME)

$(OBJ):
	mkdir -p $(OBJ)

$(NAME): $(OBJS)
	$(CC) $(FLAGS) $(THREADS) -o $(NAME) $(OBJS) -g

$(OBJ)/%.o: $(SRC)/%.c
	$(CC) $(FLAGS) $(THREADS) -o $@ -c $^ -I$(HEADER)

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all

.PHONY: all fclean clean re
