/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 10:27:56 by alangloi          #+#    #+#             */
/*   Updated: 2021/09/30 14:04:32 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

static int	launch_philo(t_philo *philo)
{
	if (!create_thread(philo, philo->param->numb))
		return (0);
	if (!join_thread(philo))
		return (0);
	return (1);
}

static int	init_philo(t_philo **philo, char **argv)
{
	t_param	*param;

	param = parsing(argv);
	*philo = create_philo(param);
	if (!param || !*philo || !init_mutex(*philo))
		return (0);
	return (1);
}

int	main(int argc, char **argv)
{
	t_philo	*philo;

	if (argc < 5 || argc > 6)
	{
		wrong_arg();
		exit(1);
	}
	if (!init_philo(&philo, argv))
		exit_philo(philo);
	if (!launch_philo(philo))
		exit_philo(philo);
	usleep(500);
	exit_philo(philo);
	return (1);
}
