/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 10:26:21 by alangloi          #+#    #+#             */
/*   Updated: 2021/10/07 18:02:00 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

static void	setup(t_philo *philo, t_param *param, int i)
{
	philo->param = param;
	philo->id = i + 1;
	philo->meals = 0;
	philo->last_meal = 0;
	if (pthread_mutex_init(&philo->_last_meal, NULL) != 0)
		return ;
	assign_fork(param, philo);
}

void	*create_philo(t_param *param)
{
	int		i;
	t_philo	*last;
	t_philo	*philo;
	t_philo	*first;

	i = 0;
	while (i < param->numb)
	{
		philo = malloc(sizeof(t_philo));
		if (!philo)
			return (NULL);
		if (i != 0)
			last->next = philo;
		else
			first = philo;
		setup(philo, param, i);
		philo->next = NULL;
		last = philo;
		i++;
	}
	return (first);
}
