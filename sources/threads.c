/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   threads.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 10:25:45 by alangloi          #+#    #+#             */
/*   Updated: 2021/10/08 16:55:24 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

int	init_mutex(t_philo *philo)
{
	if (pthread_mutex_init(&philo->param->_is_dead, NULL) != 0)
		return (0);
	if (pthread_mutex_init(&philo->param->_has_eat, NULL) != 0)
		return (0);
	if (pthread_mutex_init(&philo->param->writing, NULL) != 0)
		return (0);
	return (1);
}

static void	philo_solo(t_philo *philo)
{
	if (philo->param->numb == 1)
	{
		pthread_mutex_lock(philo->left);
		writing("has taken a fork", philo);
		pthread_mutex_unlock(philo->left);
		wait_until(philo->param->t_die);
		usleep(500);
		pthread_mutex_lock(&philo->param->_is_dead);
		philo->param->is_dead = 1;
		pthread_mutex_unlock(&philo->param->_is_dead);
	}
}

static void	*execute_thread(void *arg)
{
	t_philo	*philo;

	philo = (t_philo *)arg;
	pthread_mutex_lock(&philo->_last_meal);
	philo->last_meal = get_time();
	pthread_mutex_unlock(&philo->_last_meal);
	philo_solo(philo);
	if (philo->id % 2 == 1)
		usleep(500);
	usleep(500);
	while (check_eat(philo) && check_dead(philo))
	{
		philo_eat(philo);
		philo_sleep(philo);
		philo_think(philo);
	}
	return (NULL);
}

int	join_thread(t_philo *philo)
{
	t_philo	*cur;

	cur = philo;
	while (cur)
	{
		if (pthread_join(cur->tp, NULL) != 0)
			return (0);
		cur = cur->next;
	}
	if (pthread_join(philo->param->_is_alive, NULL) != 0)
		return (0);
	return (1);
}

int	create_thread(t_philo *philo, int nb)
{
	t_philo		*cur;
	pthread_t	*tp;
	int			i;

	i = 0;
	cur = philo;
	while (i < nb)
	{
		if (pthread_create(&cur->tp, NULL, execute_thread, (void *)cur) != 0)
			return (0);
		i++;
		cur = cur->next;
	}
	tp = &philo->param->_is_alive;
	if (pthread_create(tp, NULL, alive, (void *)philo) != 0)
		return (0);
	return (1);
}
