/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   writing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 10:25:56 by alangloi          #+#    #+#             */
/*   Updated: 2021/10/08 16:31:19 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

void	writing(char *str, t_philo *philo)
{
	long	time;

	time = get_time();
	pthread_mutex_lock(&philo->param->writing);
	pthread_mutex_lock(&philo->param->_is_dead);
	if (philo->param->is_dead == 0)
		printf("%ld %i %s\n", time - philo->param->orig_time,
			   philo->id, str);
	pthread_mutex_unlock(&philo->param->writing);
	pthread_mutex_unlock(&philo->param->_is_dead);
}
