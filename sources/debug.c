/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 10:25:00 by alangloi          #+#    #+#             */
/*   Updated: 2021/09/09 10:26:03 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

void	debug_philo(t_philo *philo)
{
	printf("--------debug philo---------\n");
	printf("- id =\t\t\t\t%i\n", philo->id);
	printf("- meals =\t\t\t%i\n", philo->meals);
	printf("- last_meal =\t\t\t%ld\n", philo->last_meal);
	printf("----------------------------\n");
}

void	debug_parsing(t_param *param)
{
	printf("--------debug parsing---------\n");
	printf("- number_of_philosophers =\t%d\n", param->numb);
	printf("- time_to_die =\t\t\t%ld\n", param->t_die);
	printf("- time_to_eat =\t\t\t%ld\n", param->t_eat);
	printf("- time_to_sleep =\t\t%ld\n", param->t_sleep);
	printf("- number_of_times =\t\t%d\n", param->nb_eat);
	printf("----------------------------\n");
}

void	debug_param(t_param *param)
{
	printf("--------debug param---------\n");
	printf("- has_eat =\t\t\t%i\n", param->has_eat);
	printf("- is_dead =\t\t\t%i\n", param->is_dead);
	printf("----------------------------\n");
}
