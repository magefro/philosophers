/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   forks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 10:26:11 by alangloi          #+#    #+#             */
/*   Updated: 2021/10/07 16:13:09 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

int	create_fork(t_param *param)
{
	int				i;
	pthread_mutex_t	*fork;

	i = 0;
	fork = malloc(sizeof(pthread_mutex_t) * param->numb);
	if (fork == NULL)
		return (0);
	while (i < param->numb)
	{
		if (pthread_mutex_init(&fork[i], NULL) != 0)
			return (0);
		i++;
	}
	param->fork = fork;
	return (1);
}

void	assign_fork(t_param *param, t_philo *philo)
{
	if (philo->id == philo->param->numb)
		philo->right = &philo->param->fork[0];
	else
		philo->right = &philo->param->fork[philo->id];
	philo->left = &philo->param->fork[(philo->id - 1) % param->numb];
}
