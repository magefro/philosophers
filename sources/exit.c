/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit_philo.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 10:25:19 by alangloi          #+#    #+#             */
/*   Updated: 2021/09/18 00:05:41 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

static void	mutex_destroy(t_philo *philo)
{
	pthread_mutex_destroy(&philo->param->_is_dead);
	pthread_mutex_destroy(&philo->param->_has_eat);
	pthread_mutex_destroy(&philo->param->writing);
}

int	exit_philo(t_philo *philo)
{
	t_philo	*tmp;
	int		i;

	i = 0;
	while (i < philo->param->numb)
	{
		pthread_mutex_destroy(&philo->param->fork[i]);
		i++;
	}
	free(philo->param->fork);
	mutex_destroy(philo);
	free(philo->param);
	tmp = philo;
	while (philo != NULL)
	{
		tmp = philo->next;
		pthread_mutex_destroy(&philo->_last_meal);
		free(philo);
		philo = tmp;
	}
	exit(0);
}
